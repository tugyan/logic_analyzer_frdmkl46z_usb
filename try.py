import hid
import time

for d in hid.enumerate():
    keys = d.keys()
    for key in keys:
        print("{} : {}".format(key, d[key]))
    print("")

try:
    print("Opening device")
    h = hid.device()
    h.open(8137, 127)

    print("Manufacturer: %s" % h.get_manufacturer_string())
    print("Product: %s" % h.get_product_string())
    print("Serial No: %s" % h.get_serial_number_string())

    # try non-blocking mode by uncommenting the next line
    #h.set_nonblocking(1)

    # try writing some data to the device

    h.write([0x00])
    d = h.read(8)
    if d:
        print("".join(chr(val) for val in d))
    time.sleep(0.05)

    print("Closing device")
    h.close()

except IOError:
    print("You probably don't have the hard coded test hid. Update the hid.device line")
    print("in this script with one from the enumeration list output above and try again.")

print("Done")
