/*
 * Copyright (c) 2013 - 2016, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * FRDM-KL46Z Logic Probe Code.
 * Author: Bilal Taşdelen
 **/

#include "pin_mux.h"
#include "fsl_debug_console.h"
#include "SerialComm.h"


void BOARD_InitHardware(void);
void BOARD_DbgConsole_Deinit(void);
void BOARD_DbgConsole_Init(void);

/*!
 * @brief Application initialization function.
 *
 * This function initializes the application.
 *
 * @return None.
 */
void APPInit(void)
{
    uint8_t irqNo;

#if defined(USB_DEVICE_CONFIG_KHCI) && (USB_DEVICE_CONFIG_KHCI > 0)
    uint8_t khciIrq[] = USB_IRQS;
    irqNo = khciIrq[CONTROLLER_ID - kUSB_ControllerKhci0];

    SystemCoreClockUpdate();

    CLOCK_EnableUsbfs0Clock(kCLOCK_UsbSrcPll0, CLOCK_GetFreq(kCLOCK_PllFllSelClk));
#endif

    s_cdcVcom.speed = USB_SPEED_FULL;
    s_cdcVcom.attach = 0;
    s_cdcVcom.deviceHandle = NULL;

    if (kStatus_USB_Success != USB_DeviceInit(CONTROLLER_ID, USB_DeviceCallback, &s_cdcVcom.deviceHandle))
    {
        usb_echo("USB device vcom failed\r\n");
        return;
    }
    else
    {
        usb_echo("USB device CDC virtual com demo\r\n");
    }

    NVIC_SetPriority((IRQn_Type)irqNo, USB_DEVICE_INTERRUPT_PRIORITY);
    NVIC_EnableIRQ((IRQn_Type)irqNo);

    USB_DeviceRun(s_cdcVcom.deviceHandle);


}

/*!
 * @brief Application entry point.
 */
int main(void) {
  /* Init board hardware. */
  BOARD_InitPins();
  BOARD_BootClockRUN();
  BOARD_InitDebugConsole();

  LED_GREEN_INIT(0);
  LED_GREEN_OFF();

  LED_RED_INIT(0);
  LED_RED_OFF();

  APPInit();


  initChannels();

  /* Add your code here */
  comm_mode_t currMode = IDLE;
  comm_config_state_t currState = ERR_UNKNOWN_CONFIG;
  config_status_t cfg_stat = UNKNOWN_CMD;
  for(;;) { /* Infinite loop to avoid leaving the main function */
	  switch (currMode) {
		case IDLE:
			LED_GREEN_ON();
			LED_RED_OFF();
			currMode = changeMode();
			break;

		case CONFIG_MODE:
			LED_GREEN_OFF();
			LED_RED_ON();
			currState = changeConfigMode();
			switch (currState) {
				case REPLY_CMD:
					cfg_stat = replyCmd();
					break;

				case SET_CONFIG:
					cfg_stat = applyCmd();
					break;

				case ERR_UNKNOWN_CONFIG:
					cfg_stat = UNKNOWN_CMD;
					break;
			}
			if(cfg_stat == SUCCESS)	currMode = IDLE;
			else currMode = ERR_MODE;
		case SAMPLING_MODE:
			LED_RED_ON();
			startSampling();
			while(isSampling) {
				__asm("NOP");
			}
			LED_RED_OFF();
			currMode = IDLE;
			break;

		case ERR_MODE:
		default:
			LED_GREEN_OFF();
			LED_RED_ON();
			currMode = IDLE;
			s_currSendBuf[0] = SEND_ERR;
			s_currSendBuf[1] = cfg_stat;
			USB_DeviceSendRequest(s_cdcVcom.deviceHandle, USB_CDC_VCOM_BULK_IN_ENDPOINT, s_currSendBuf, ERR_MSG_SIZE);
			break;
	}
  }
}
