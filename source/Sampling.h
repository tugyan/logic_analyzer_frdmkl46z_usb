#include "board.h"
#include <stdlib.h>
#include <math.h>
#include "fsl_dma.h"
#include "fsl_dmamux.h"
#include "virtual_com.h"
#include "fsl_pit.h"

#define MAX_SAMPLING_FREQ 12000000
#define CH_GPIO GPIOC
#define SAMPLING_TIMER kPIT_Chnl_0
#define MAX_CH_NUM 8
#define AVAIL_MEM_SIZE 20000

uint8_t active_ch_num = MAX_CH_NUM;

volatile bool isSampling = false;
uint32_t samplingFreq = MAX_SAMPLING_FREQ;
uint8_t dataBuffer[AVAIL_MEM_SIZE];
uint32_t samplingDuration = 40000;

void initChannels(void);
void startSampling(void);
bool setSamplingFrequency(uint32_t);
void sampleChannels(void);
